<?php

namespace Drupal\entity_visitors\Service;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\entity_visitors\Entity\EntityVisitors;
use Drupal\entity_visitors\Event\EntityVisitedEvent;
use Drupal\Core\Config\ConfigManagerInterface;

/**
 * Class EntityVisitorsManager.
 */
class EntityVisitorsManager {

  /**
   * Drupal\Core\Config\ConfigManagerInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  private $currentUser;

  /**
   * The matched route.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  private $routeMatch;

  /**
   * The event dispatcher service.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  private $eventDispatcher;

  /**
   * The entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The entityTypeBundleInfo service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  private $entityTypeBundleInfo;

  /**
   * Constructs a new EntityVisitorsManager object.
   */
  public function __construct(ConfigManagerInterface $config_manager,
                              AccountProxy $currentUser,
                              RouteMatchInterface $routeMatch,
                              ContainerAwareEventDispatcher $eventDispatcher,
                              EntityTypeManagerInterface $entityTypeManager,
                              EntityTypeBundleInfo $entityTypeBundleInfo) {

    $this->configManager = $config_manager;
    $this->currentUser = $currentUser;
    $this->routeMatch = $routeMatch;
    $this->eventDispatcher = $eventDispatcher;
    $this->entityTypeManager = $entityTypeManager;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
  }

  /**
   * Handle the visited entity, the main logic is here.
   */
  public function handleEntity($routeName, $visitedEntityType, $visitedEntityId) {
    $entityVisitor = $this->currentUser;
    $entityVisitorId = $this->currentUser->id();
    if (!$entityVisitor->isAuthenticated()) {
      return;
    }
    $bundles = $this->entityTypeBundleInfo->getBundleInfo($visitedEntityType);
    // If a bundlable entity.
    if (count($bundles) > 1 || $visitedEntityType == 'node') {
      // This a bundlable content type.
      // then append the bundle name to the entity name.
      // eg, node.page or node.article.
      // this can be used later in views.
      $bundleName = $this->routeMatch->getParameter($visitedEntityType)
        ->bundle();
      $visitedEntityType .= '.' . $bundleName;
    }
    // Handle if user visiting himself.
    if ($visitedEntityType == 'user' && $visitedEntityId === $entityVisitorId) {
      return;
    }
    // Excluded roles won't be counted.
    $excludedRoles = $this->configManager->getConfigFactory()
      ->get('entity_visitors.entityvisitiorsconfig')
      ->get('excluded_roles');
    if (isset($excludedRoles) && array_intersect($entityVisitor->getRoles(), $excludedRoles)) {
      return;
    }
    // Dispatch event entityVisited to use it for things
    // like creating a message that an entity has reached
    // a certain number views or pretty much anything.
    $entityVisitedEvent = new EntityVisitedEvent($visitedEntityId, $visitedEntityType, $entityVisitorId);
    // Dispatch event, an entity was visited!
    $this->eventDispatcher->dispatch(EntityVisitedEvent::VISITED, $entityVisitedEvent);

    $saveLastVisitOnly = $this->configManager->getConfigFactory()
      ->get('entity_visitors.entityvisitiorsconfig')
      ->get('save_last_visits_only');
    // As per https://www.drupal.org/project/entity_vistiors/issues/3125065#comment-13551826
    // there will be advanced option that will keep a history of everything
    // and a basic option that will update the created entity with a new date.
    if ($saveLastVisitOnly) {
      $visitedEntityExist = $this->entityTypeManager->getStorage('entity_visitors')
        ->getQuery('AND')
        ->condition('field_visited_entity_id', $visitedEntityId)
        ->condition('name', $visitedEntityType)
        ->condition('field_entity_visitor', $entityVisitorId)
        ->execute();
      // This has entity has previously been created.
      if (!empty($visitedEntityExist)) {
        $existedVisitedEntity = EntityVisitors::load(array_values($visitedEntityExist)[0]);
        $existedVisitedEntity->field_entity_visiting_time = \Drupal::time()
          ->getCurrentTime();
        $existedVisitedEntity->save();
        return;
      }
    }
    // This has entity has previously been created.
    EntityVisitors::create([
      'field_entity_visitor' => $entityVisitorId,
      'name' => $visitedEntityType,
      'field_visited_entity_id' => $visitedEntityId,
      'field_entity_visiting_time' => \Drupal::time()->getCurrentTime(),
    ])->save();

  }

}
