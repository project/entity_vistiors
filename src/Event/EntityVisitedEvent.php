<?php

namespace Drupal\entity_visitors\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * EntityVisted Event Class.
 *
 * This class is creating an event to be dispatched when user
 * visits and entity.
 */
class EntityVisitedEvent extends Event {

  const VISITED = 'entity_visited.event';

  /**
   * The id of the visited entity, like the node id, user id ,etc,.
   *
   * @var visitedEntityId
   */
  public $visitedEntityId;
  /**
   * The Visited Entity type string.
   *
   * @var visitedEntityType
   *   The entity type can be entity_type.bundle_name like node.event,
   *   'node.page' if it has bundles or only the entity type name
   *   like 'user' if it has no bundles.
   */
  public $visitedEntityType;
  /**
   * The id of the visitor ($user)
   *
   * @var visitorId
   */
  public $visitorId;

  /**
   * EntityVisitedEvent constructor.
   *
   * @param int $visitedEntityId
   *   Pass the visited entity id.
   * @param string $visitedEntityType
   *   Pass the entity type string. eg, node or user.
   * @param int $visitorId
   *   The user visited this entity.
   */
  public function __construct($visitedEntityId, $visitedEntityType, $visitorId) {
    $this->visitedEntityId = $visitedEntityId;
    $this->visitedEntityType = $visitedEntityType;
    $this->visitorId = $visitorId;
  }

}
