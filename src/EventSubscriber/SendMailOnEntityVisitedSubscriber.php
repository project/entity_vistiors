<?php

namespace Drupal\entity_visitors\EventSubscriber;

use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\entity_visitors\Event\EntityVisitedEvent;
use Drupal\user\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * A subscriber to the route when it matches an entity route.
 */
class SendMailOnEntityVisitedSubscriber implements EventSubscriberInterface {

  /**
   * A mail manager instance.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  private $mailManager;

  /**
   * A configuration manager instance.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  private $configManager;

  /**
   * {@inheritDoc}
   */
  public function __construct(MailManagerInterface $mailManager, ConfigManagerInterface $configManager) {
    $this->mailManager = $mailManager;
    $this->configManager = $configManager;
  }

  /**
   * Listen to the requests to check the current route.
   */
  public static function getSubscribedEvents() {
    return [
      EntityVisitedEvent::VISITED => 'sendMail',
    ];
  }

  /**
   * Send mail on entity visited.
   */
  public function sendMail(EntityVisitedEvent $event) {
    if ($event->visitedEntityType == "user") {
      $user = User::load($event->visitedEntityId);
      $allowedRoles = $this->configManager->getConfigFactory()
        ->get('entity_visitors.entityvisitiorsconfig')
        ->get('enable_mail_roles');
      if (!empty($allowedRoles) && array_intersect($user->getRoles(), $allowedRoles)) {
        $params = [
          "subject" => $this->configManager->getConfigFactory()
            ->get('entity_visitors.entityvisitiorsconfig')
            ->get('mail_subject'),
          "body" => $this->configManager->getConfigFactory()
            ->get('entity_visitors.entityvisitiorsconfig')
            ->get('mail_template')["value"],
        ];
        $this->mailManager->mail("entity_visitors", "mail_to_visited_user", $user->getEmail(), $user->getPreferredLangcode(), $params);
      }
    }
  }

}
