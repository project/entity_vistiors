<?php

namespace Drupal\entity_visitors;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for entity_visitors.
 */
class EntityVisitorsTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
